//
//  TelemetryManager.swift
//  AdblockPlusSafari
//
//  Created by Florian Paillard on 04/03/2025.
//  Copyright © 2025 eyeo GmbH. All rights reserved.
//

import Foundation

/// Manages telemetry data collection and reporting for eyeo products
class TelemetryManager {
    // MARK: - Properties

    /// Singleton instance
    static let shared = TelemetryManager()

    /// URL session for network requests
    private let urlSession: URLSession

    /// User defaults for storing ping data
    private let userDefaults: UserDefaults

    /// Telemetry host from Info.plist
    private let host: String

    /// Telemetry token from Info.plist
    private let token: String

    /// API path for the telemetry endpoint
    private let path = "/topic/companionapp_activeping/version/1"

    /// Date formatter for ISO8601 dates, truncated to day
    private lazy var dateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withDashSeparatorInDate, .withColonSeparatorInTime, .withTimeZone]
        return formatter
    }()

    // MARK: - Initialization

    /// Initialize with the specified parameters
    init(urlSession: URLSession = URLSession.shared,
         userDefaults: UserDefaults = .shared) {

        // Populate host and token from Info.plist
        self.host = Bundle.main.object(forInfoDictionaryKey: "EyeometryHost") as? String ?? ""
        self.token = Bundle.main.object(forInfoDictionaryKey: "EyeometryToken") as? String ?? ""

        if host.isEmpty || token.isEmpty {
            NSLog("Host or token is missing from Info.plist. Telemetry will not be sent.\n Host: \(host)\n Token: \(token)")
        }

        self.urlSession = urlSession
        self.userDefaults = userDefaults
    }

    // MARK: - Public Methods
    /// Send a ping to the telemetry service
    /// - Parameter completion: An optional completion handler that is called when the ping completes (true if ping was sent successfully)
    func sendPing(completion: ((Bool) -> Void)? = nil) {
        guard canSendTelemetry() else {
            NSLog("🤖 DEBUG: ping already sent today")
            completion?(false)
            return
        }
        NSLog("🤖 DEBUG: sending ping ...")

        let url = buildURL()
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")

        do {
            let payload = buildPayload()
            let jsonData = try JSONSerialization.data(withJSONObject: payload, options: [])
            request.httpBody = jsonData

            let task = urlSession.dataTask(with: request) { [weak self] data, response, error in
                guard let self = self else { return }

                // Check for network errors
                if let error = error {
                    NSLog("🔥 ERROR: Telemetry ping failed: \(error.localizedDescription)")
                    completion?(false)
                    return
                }

                // Check for HTTP status code
                guard let httpResponse = response as? HTTPURLResponse else {
                    NSLog("🔥 ERROR: Telemetry ping failed: Invalid response")
                    completion?(false)
                    return
                }

                if httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 {
                    // Successful response
                    self.handleSuccessfulPing(data)
                    completion?(true)
                    NSLog("🤖 DEBUG: ping sent!")
                } else {
                    // Error response
                    NSLog("🔥 ERROR: Telemetry ping failed with status code: \(httpResponse.statusCode)")
                    completion?(false)
                }
            }

            task.resume()
        } catch {
            NSLog("🔥 ERROR: Failed to build telemetry payload: \(error.localizedDescription)")
            completion?(false)
        }
    }

    // MARK: - Private Methods

    /// Handle a successful ping response
    /// - Parameter data: Response data
    private func handleSuccessfulPing(_ data: Data?) {
        guard let data = data else { return }
        do {
            // Parse the response token
            if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any],
               let tokenString = json["token"] as? String,
               let tokenDate = dateFormatter.date(from: tokenString) {
                // Store the previous last ping
                if let lastPingString = userDefaults.string(forKey: Constants.lastPingKey) {
                    userDefaults.set(lastPingString, forKey: Constants.previousLastPingKey)
                }
                // Store the new last ping (truncated to day)
                if let truncatedDate = tokenDate.truncateToDay() {
                    let truncatedDateString = dateFormatter.string(from: truncatedDate)
                    userDefaults.set(truncatedDateString, forKey: Constants.lastPingKey)
                    
                    // Store first ping key if needed
                    if userDefaults.string(forKey: Constants.firstPingKey) == nil {
                        userDefaults.set(truncatedDateString, forKey: Constants.firstPingKey)
                    }
                }
                // Generate and store a new UUID for last_ping_tag
                let uuid = UUID().uuidString
                userDefaults.set(uuid, forKey: Constants.lastPingTagKey)
                
            }
        } catch {
            NSLog("🔥 ERROR: Failed to parse telemetry response: \(error.localizedDescription)")
        }
    }

    /// Build the URL for the telemetry endpoint
    /// - Returns: The telemetry endpoint URL
    private func buildURL() -> URL {
        let urlString = "https://\(host)\(path)"
        return URL(string: urlString)!
    }

    /// Build the telemetry payload
    /// - Returns: A dictionary containing the telemetry payload
    internal func buildPayload() -> [String: Any] {
        var payloadData: [String: Any] = [
            "os": "iOS",
            "os_version": getOSVersion(),
            "application": "AdblockPlusSafari",
            "application_version": getApplicationVersion(),
            "aa_active": isAcceptableAdsActive(),
            "has_user_allowlist": isAllowlistUsed()
        ]

        // Add first ping if available
        if let firstPingString = userDefaults.string(forKey: Constants.firstPingKey) {
            payloadData["first_ping"] = firstPingString
        }

        // Add last ping and last ping tag if available
        if let lastPingString = userDefaults.string(forKey: Constants.lastPingKey),
           let lastPingTag = userDefaults.string(forKey: Constants.lastPingTagKey) {
            payloadData["last_ping"] = lastPingString
            payloadData["last_ping_tag"] = lastPingTag
        }

        // Add previous last ping if available
        if let previousLastPingString = userDefaults.string(forKey: Constants.previousLastPingKey) {
            payloadData["previous_last_ping"] = previousLastPingString
        }

        return ["payload": payloadData]
    }

    private func canSendTelemetry() -> Bool {
        // If we can't determine the date, allow sending telemetry
        guard let truncatedDate = Date().truncateToDay() else { return true }
        let truncatedDateString = dateFormatter.string(from: truncatedDate)
        if truncatedDateString == userDefaults.string(forKey: Constants.lastPingKey) {
            return false
        }
        return true
    }

    /// Get the OS version
    /// - Returns: The current OS version as a string
    private func getOSVersion() -> String {
        return ProcessInfo.processInfo.operatingSystemVersionString
    }

    /// Get the application version
    /// - Returns: The current application version
    private func getApplicationVersion() -> String {
        guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
            return "unknown"
        }
        return version
    }

    /// Check if Acceptable Ads are active
    /// - Returns: Whether Acceptable Ads are active
    private func isAcceptableAdsActive() -> String {
        // Use .object rather than .bool as we store it this way for historical reasons.
        // using bool(forKey:) will always return false.
        guard let acceptableAdsEnabled = userDefaults.object(forKey: Constants.acceptableAdsEnabled) as? Bool else {
            return "false"
        }
        return acceptableAdsEnabled ? "true" : "false"
    }

    /// Check if user maintained allowlist is used
    /// - Returns: A string `false` if allowlist is empty, else returns `true`
    private func isAllowlistUsed() -> String {
        let allowlistArray = userDefaults.array(forKey: Constants.allowlistArray) as? [String] ?? []
        return allowlistArray.isEmpty ? "false" : "true"
    }
}

extension Date {

    func truncateToDay() -> Date? {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: components)
    }

}
