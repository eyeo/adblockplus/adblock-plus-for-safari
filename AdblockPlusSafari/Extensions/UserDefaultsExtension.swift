//
//  UserDefaultsExtension.swift
//  AdblockPlusSafari
//
//  Created by Dean Murphy on 04/03/2025.
//  Copyright © 2025 eyeo GmbH. All rights reserved.
//

import Foundation

public extension UserDefaults {
    static var shared: UserDefaults {
        guard let defaults = UserDefaults(suiteName: Constants.groupIdentifier) else {
            NSLog("🤖 ERROR: Unable to create UserDefaults with suite name: \(Constants.appIdentifier)")
            fatalError("Failed to create UserDefaults with suite name: \(Constants.appIdentifier)")
        }
        return defaults
    }
}
