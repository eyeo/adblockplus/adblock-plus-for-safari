//
//  TelemetryManagerTests.swift
//  AdblockPlusSafari
//
//  Created by Florian Paillard on 04/03/2025.
//  Copyright © 2025 eyeo GmbH. All rights reserved.
//

@testable import AdblockPlusSafari
import XCTest

class TelemetryManagerTests: XCTestCase {

    private var telemetryManager: TelemetryManager!
    private let groupDefaults = UserDefaults(suiteName: Constants.groupIdentifier)!

    override func setUp() {
        super.setUp()
        groupDefaults.removePersistentDomain(forName: Constants.groupIdentifier)
        groupDefaults.synchronize()
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        sessionConfiguration.protocolClasses = [URLProtocolMock.self]
        let mockURLSession = URLSession(configuration: sessionConfiguration)
        telemetryManager = TelemetryManager(urlSession: mockURLSession, userDefaults: groupDefaults)

    }

    override func tearDown() {
        groupDefaults.removePersistentDomain(forName: Constants.groupIdentifier)
        groupDefaults.synchronize()
        super.tearDown()
    }

    // MARK: - Tests

    func testFirstPingPayload() {
        // Test the payload for the first ping (when no previous data exists)
        let payload = telemetryManager.buildPayload()
        let innerPayload = payload["payload"] as? [String: Any]

        // Check required fields
        XCTAssertNotNil(innerPayload?["os"])
        XCTAssertNotNil(innerPayload?["os_version"])
        XCTAssertNotNil(innerPayload?["application"])
        XCTAssertNotNil(innerPayload?["application_version"])
        XCTAssertNotNil(innerPayload?["aa_active"])

        // Check that first_ping, last_ping, and previous_last_ping are not present
        XCTAssertNil(innerPayload?["first_ping"])
        XCTAssertNil(innerPayload?["last_ping"])
        XCTAssertNil(innerPayload?["last_ping_tag"])
        XCTAssertNil(innerPayload?["previous_last_ping"])
    }

    func testSendPing_Success() {
        let testUrl = URL(string: "https://apple.test-telemetry.data.eyeo.it/topic/companionapp_activeping/version/1")!
        let response = HTTPURLResponse(url: testUrl, statusCode: 200, httpVersion: nil, headerFields: nil)
        let error: Error? = nil
        let data = String("""
        {
            "token": "2023-05-18T12:50:00Z"
        }
        """).data(using: .utf8)

        URLProtocolMock.mockURLs = [
            testUrl: (error, data, response)
        ]

        // Create expectation
        let expectation = self.expectation(description: "Ping sent successfully")

        // Execute ping with completion handler for testing
        telemetryManager.sendPing(completion: { success in
            XCTAssertTrue(success)
            expectation.fulfill()
        })

        // Wait for async operation
        waitForExpectations(timeout: 1.0, handler: nil)

        // Verify first_ping was set
        let firstPing = groupDefaults.string(forKey: Constants.firstPingKey)
        XCTAssertNotNil(firstPing)
        XCTAssertTrue(firstPing?.hasSuffix("T00:00:00Z") ?? false)
        XCTAssertTrue(firstPing?.hasPrefix("2023-05-18") ?? false)

        // Verify last_ping was set and truncated to day
        let lastPing = groupDefaults.string(forKey: Constants.lastPingKey)
        XCTAssertNotNil(lastPing)
        XCTAssertTrue(lastPing?.hasSuffix("T00:00:00Z") ?? false)

        // Verify last_ping_tag was generated
        let lastPingTag = groupDefaults.string(forKey: Constants.lastPingTagKey)
        XCTAssertNotNil(lastPingTag)
    }

    func testSendPing_Error() {
        let testUrl = URL(string: "https://apple.test-telemetry.data.eyeo.it/topic/companionapp_activeping/version/1")!
        let response = HTTPURLResponse(url: testUrl, statusCode: 500, httpVersion: nil, headerFields: nil)
        URLProtocolMock.mockURLs = [
            testUrl: (nil, nil, response)
        ]

        // Create expectation
        let expectation = self.expectation(description: "Ping failed")

        // Execute ping with completion handler for testing
        telemetryManager.sendPing(completion: { success in
            XCTAssertFalse(success)
            expectation.fulfill()
        })

        // Wait for async operation
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func testSecondPingPayload() {
        // Simulate first successful ping
        groupDefaults.set("2023-05-24T00:00:00Z", forKey: Constants.firstPingKey)
        groupDefaults.set("2023-05-24T00:00:00Z", forKey: Constants.lastPingKey)
        groupDefaults.set("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee", forKey: Constants.lastPingTagKey)

        // Get the payload for the second ping
        let payload = telemetryManager.buildPayload()
        let innerPayload = payload["payload"] as? [String: Any]

        // Check that fields are properly included
        XCTAssertEqual(innerPayload?["first_ping"] as? String, "2023-05-24T00:00:00Z")
        XCTAssertEqual(innerPayload?["last_ping"] as? String, "2023-05-24T00:00:00Z")
        XCTAssertEqual(innerPayload?["last_ping_tag"] as? String, "aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee")
        XCTAssertNil(innerPayload?["previous_last_ping"])
    }

    func testThirdPingPayload() {
        // Simulate first and second successful pings
        groupDefaults.set("2023-05-24T00:00:00Z", forKey: Constants.firstPingKey)
        groupDefaults.set("2023-05-25T00:00:00Z", forKey: Constants.lastPingKey)
        groupDefaults.set("ffffffff-bbbb-cccc-dddd-ffffffffffff", forKey: Constants.lastPingTagKey)
        groupDefaults.set("2023-05-24T00:00:00Z", forKey: Constants.previousLastPingKey)

        // Get the payload for the third ping
        let payload = telemetryManager.buildPayload()
        let innerPayload = payload["payload"] as? [String: Any]

        // Check that fields are properly included
        XCTAssertEqual(innerPayload?["first_ping"] as? String, "2023-05-24T00:00:00Z")
        XCTAssertEqual(innerPayload?["last_ping"] as? String, "2023-05-25T00:00:00Z")
        XCTAssertEqual(innerPayload?["last_ping_tag"] as? String, "ffffffff-bbbb-cccc-dddd-ffffffffffff")
        XCTAssertEqual(innerPayload?["previous_last_ping"] as? String, "2023-05-24T00:00:00Z")
    }

}

// MARK: - Test Helpers

class URLProtocolMock: URLProtocol {
    /// Dictionary maps URLs to tuples of error, data, and response
    static var mockURLs = [URL?: (error: Error?, data: Data?, response: HTTPURLResponse?)]()

    override class func canInit(with request: URLRequest) -> Bool {
        // Handle all types of requests
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        // Required to be implemented here. Just return what is passed
        return request
    }

    override func startLoading() {
        if let url = request.url {
            if let (error, data, response) = URLProtocolMock.mockURLs[url] {

                // We have a mock response specified so return it.
                if let responseStrong = response {
                    self.client?.urlProtocol(self, didReceive: responseStrong, cacheStoragePolicy: .notAllowed)
                }

                // We have mocked data specified so return it.
                if let dataStrong = data {
                    self.client?.urlProtocol(self, didLoad: dataStrong)
                }

                // We have a mocked error so return it.
                if let errorStrong = error {
                    self.client?.urlProtocol(self, didFailWithError: errorStrong)
                }
            }
        }

        // Send the signal that we are done returning our mock response
        self.client?.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() {
        // Required to be implemented. Do nothing here.
    }
}
