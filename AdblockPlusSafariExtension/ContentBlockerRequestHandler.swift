/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import MobileCoreServices
import UIKit

class ContentBlockerRequestHandler: NSObject, NSExtensionRequestHandling {
    private let groupDefaults: UserDefaults = .shared

    /// Checks if Acceptable Ads setting is enabled.
    ///
    /// - Returns: Filterlist filename from App Group Storage.
    private func getBlocklistName() -> String {
        // Return the test filterlist if user disables the blockAds Switch.
        let blockAdsEnabled = groupDefaults.object(forKey: Constants.blockAdsEnabled) as? Bool
        NSLog("🤖 DEBUG: blockAdsEnabled = \(String(describing: blockAdsEnabled))")

        if blockAdsEnabled == false {
            return Constants.sharedFilterlistTest
        }

        // Used .object rather than .bool as .bool returns false if key doesnt exist, rather than nil.
        let acceptableAdsEnabled = groupDefaults.object(forKey: Constants.acceptableAdsEnabled) as? Bool
        NSLog("🤖 DEBUG: acceptableAdsEnabled = \(String(describing: acceptableAdsEnabled))")

        guard let acceptableAdsEnabled = acceptableAdsEnabled else {
            return Constants.sharedFilterlistPlusExceptionRules
        }
        switch acceptableAdsEnabled {
        case true:
            return Constants.sharedFilterlistPlusExceptionRules
        case false:
            return Constants.sharedFilterlist
        }
    }

    /// Checks if Acceptable Ads setting is enabled.
    ///
    /// - Returns: Filterlist filename from app bundle.
    private func getBundleFilterlistName() -> String {
        let acceptableAdsEnabled = groupDefaults.object(forKey: Constants.acceptableAdsEnabled) as? Bool ?? true
        return acceptableAdsEnabled ? Constants.easylistExceptionRules : Constants.easylist
    }

    func beginRequest(with context: NSExtensionContext) {
        NSLog("🤖 DEBUG: Starting beginRequest")
        TelemetryManager.shared.sendPing()

        let fileManager = FileManager.default
        let blocklistName = getBlocklistName()
        let blocklistURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Constants.groupIdentifier)
        let blocklistPath = URL(string: "\(blocklistName).json", relativeTo: blocklistURL)

        let allowlistArray = groupDefaults.stringArray(forKey: Constants.allowlistArray) ?? [String]()

        // If blocklists are stored at local path and allowlist isn't empty, then we read from app group storage.
        // Else if file does not exist, or allowlist is empty, we read directly from app bundle.
        // This is a workaround to ensure the presumed majority of users who don't update their allowlist are presented with the
        // latest filterlist from the app bundle, rather than the one set in the filesystem when they open the app.
        if fileManager.fileExists(atPath: blocklistPath?.path ?? "") && !allowlistArray.isEmpty {
            let attachment = NSItemProvider(contentsOf: blocklistPath)!
            let item = NSExtensionItem()
            item.attachments = [attachment]
            NSLog("🤖 DEBUG: providing filterlist from app group storage: \(String(describing: blocklistPath?.path))")
            context.completeRequest(returningItems: [item], completionHandler: nil)
        } else {
            let filterlistName = getBundleFilterlistName()
            NSLog("🤖 DEBUG: providing filterlist from app bundle: \(filterlistName)")
            let attachment = NSItemProvider(contentsOf: Bundle.main.url(forResource: filterlistName, withExtension: "json"))!
            let item = NSExtensionItem()
            item.attachments = [attachment]
            context.completeRequest(returningItems: [item], completionHandler: nil)
        }
    }
}
